# Tugas​ ​1 Pengembangan​ ​Aplikasi​ ​Web​ ​dengan​ TDD,​ ​Django,​ ​Models​ ​,​ ​HTML,​ ​CSS

## Oleh:
1. Bram Sedana Wehantouw  - 1606824502
2. Degoldie Sonny         - 1606862702
3. Hilya Auli Fesmia      - 1606890933
4. Nicholas Priambodo     - 1606879905

[![pipeline status](https://gitlab.com/kelompok-8A/fesbuk/badges/master/pipeline.svg)](https://gitlab.com/kelompok-8A/fesbuk/commits/master)

[![coverage report](https://gitlab.com/kelompok-8A/fesbuk/badges/master/coverage.svg)](https://gitlab.com/kelompok-8A/fesbuk/commits/master)

## Herokuapp:
https://fesbuk.herokuapp.com/
