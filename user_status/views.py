from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Status
from .forms import Status_Form
from user_profile.models import Profile

# Create your views here.
response = {}

def index(request):
    profile = Profile.objects.get(pk=1)
    response['profile'] = profile
    response['list'] = Status.objects.all()
    response['update_status_form'] = Status_Form
    html = "user_status/user_status.html"
    return render(request, html, response)

def add_status(request):
    if(request.method == 'POST' and Status_Form(request.POST or None).is_valid()):
        Status(status=request.POST['status']).save()
        return HttpResponseRedirect('/status/')
    else:
        return HttpResponseRedirect('/status/')
