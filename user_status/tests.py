from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, add_status
from user_profile.models import Profile
from .models import Status
from .forms import Status_Form
from datetime import date

# Create your tests here.
class StatusUnitTest(TestCase):
    def setUp(self):
        profile = Profile.objects.create(name="Guest", birthday=date(1900, 1, 1))

    def test_status_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_status_using_index_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, index)

    def test_status_using_add_status_func(self):
        found = resolve('/status/add-status')
        self.assertEqual(found.func, add_status)

    def test_status_using_update_status_template(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'user_status/user_status.html')

    def test_page_using_name_from_profile(self):
        profile = Profile.objects.all()[0]

        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')

        self.assertIn(profile.name, html_response)

    def test_root_url_now_is_using_index_page_from_status(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response, '/status/', 301, 200)

    def test_model_can_create_status(self):
        status = Status.objects.create(status="Hei hei siapa dia?")
        counting_all_status = Status.objects.all().count()
        self.assertEqual(counting_all_status, 1)

    def test_form_update_status_has_placeholder_and_css_classes(self):
        form = Status_Form()
        self.assertIn('class="status-form-textarea', form.as_p())
        self.assertIn('id="id_status', form.as_p())

    def test_status_post_success_and_render_the_result(self):
        status = "love the confidence, even love a quarter more"
        response_post = Client().post('/status/add-status', {'status' : status})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertIn(status, html_response)

    def test_status_form_validation_for_blank_items(self):
        status_form = Status_Form(data={'status' : ''})
        self.assertFalse(status_form.is_valid())
        self.assertEqual(status_form.errors['status'], ["This field is required."])

    def test_status_post_error_and_render_the_result(self):
        status = "hate"
        response_post = Client().post('/status/add-status', {'status' : ''})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(status, html_response)
