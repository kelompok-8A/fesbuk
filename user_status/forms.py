from django import forms

class Status_Form(forms.Form):
    error_messages = {
    'required' : 'Tolong isi input ini'
    }

    status_attrs = {
    'type' : 'text',
    'placeholder' : "Masukkan status...",
    'cols' : 50,
    'rows' : 4,
    'class' : 'status-form-textarea'
    }

    status = forms.CharField(label='', required=True, max_length=150, widget=forms.Textarea(attrs=status_attrs))
