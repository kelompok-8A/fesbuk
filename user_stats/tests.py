import sys
sys.path.append(sys.path[0] + '/..')

from django.test import TestCase
from django.test import Client
from django.utils import timezone

from unittest import skip

from user_status.models import Status
from user_friends.models import Friend
from user_profile.models import Profile
from datetime import date

# Create your tests here.
class UnitTest(TestCase):
	def setUp(self):
		profile = Profile.objects.create(name="Guest", birthday=date(1900, 1, 1))

	def test_stats_is_exist(self):
		response = Client().get('/stats/')
		self.assertEqual(response.status_code, 200)

	def test_stats_has_status_count(self):
		status_dummy = Status.objects.create(status='dummy status')

		response = Client().get('/stats/')
		html_response = response.content.decode('utf8')

		self.assertIn('1 Posts', html_response)

	def test_stats_has_friend_count(self):
		friend_dummy = Friend.objects.create(name='dummy friend', url='http://dummyurl.friend/')

		response = Client().get('/stats/')
		html_response = response.content.decode('utf8')

		self.assertIn('1 People', html_response)

	def test_stats_has_latest_post(self):
		status_dummy = Status.objects.create(status='dummy status')

		response = Client().get('/stats/')
		html_response = response.content.decode('utf8')

		self.assertIn(status_dummy.status, html_response)

	def test_stats_has_no_latest_post(self):
		no_post_text = "This user hasn't posted anything yet."

		response = Client().get('/stats/')
		html_response = response.content.decode('utf8')

		self.assertIn(no_post_text, html_response)
