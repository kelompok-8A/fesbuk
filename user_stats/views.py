import sys
sys.path.append(sys.path[0] + '/..')

from django.shortcuts import render
from user_profile.views import Profile
from user_status.models import Status
from user_friends.models import Friend

response = {}

def index(request):
	profile = Profile.objects.get(pk=1)
	response['profile'] = profile

	status_count = len(Status.objects.all())
	response['status_count'] = status_count

	friend_count = len(Friend.objects.all())
	response['friend_count'] = friend_count

	qs = Status.objects.order_by('-created_date')
	if(len(qs) > 0):
		latest_post = qs[0]
	else:
		latest_post = None

	response['latest_post'] = latest_post

	html = 'user_stats/user_stats.html'

	return render(request, html, response)
