from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Friend_Form
from .models import Friend

# Create your views here.

author = "Kelompok 8A"
response = {}

def index(request):
	response["author"] = author
	html = "user_friends/user_friends.html"
	friend = Friend.objects.all()
	friend = reversed(friend)
	response['friend'] = friend
	response["friend_form"] = Friend_Form
	return render(request, html, response)

def add_friend(request):
    form = Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        friend = Friend(name=response['name'],url=response['url'])
        friend.save()
        return HttpResponseRedirect('/friends/')
    else:
        return HttpResponseRedirect('/friends/')
