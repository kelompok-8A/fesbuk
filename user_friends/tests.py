from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_friend
from .models import Friend
from .forms import Friend_Form

# Create your tests here.
class UserFriendsTest(TestCase):
    def test_user_friends_url_is_exist(self):
        response = Client().get('/friends/')
        self.assertEqual(response.status_code, 200)

    def test_user_friends_using_index_func(self):
        found = resolve('/friends/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_friend(self):
        new_friend = Friend.objects.create(name='test', url='http://fesbuk.herokuapp.com')

        counting_all_available_friend = Friend.objects.all().count()
        self.assertEqual(counting_all_available_friend, 1)

    def test_user_friends_post_success_and_render_the_result(self):
        test = 'http://fesbuk.herokuapp.com'
        response_post = Client().post('/friends/add_friend', {'name':test, 'url':test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/friends/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_user_friends_post_error_and_render_the_result(self):
        test = 'http://fesbuk.herokuapp.com'
        response_post = Client().post('/friends/add_friend', {'name':'', 'url':''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/friends/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
