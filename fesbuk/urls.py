"""fesbuk URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView

import user_status.urls as user_status
import user_profile.urls as user_profile
import user_friends.urls as user_friends
import user_stats.urls as user_stats

urlpatterns = [
	url(r'^$', RedirectView.as_view(url='status/', permanent=True), name='index'),
    url(r'^admin/', admin.site.urls),
    url(r'^status/', include(user_status,namespace='status')),
    url(r'^profile/', include(user_profile,namespace='profile')),
    url(r'^friends/', include(user_friends,namespace='friends')),
    url(r'^stats/', include(user_stats,namespace='stats')),
]
