from django.shortcuts import render
from .models import Profile

response = {}

#bio_dict = [{'subject' : 'Birthday', 'value' : ':  ' + calculate_age(birth_date)},\
    #{'subject' : 'Gender', 'value' : ':  ' + user_gender},\
	#{'subject' : 'Description', 'value' : ':  ' + user_description},\
	#{'subject' : 'Email', 'value' : ':  ' + user_mail}]

#bio_name = [{'subject' : user_name}]

def index(request):
    profile = Profile.objects.get(pk=1)
    response['profile'] = profile
    return render(request, 'user_profile/user_profile.html', response)
