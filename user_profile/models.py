from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Profile(models.Model):
    name = models.CharField(max_length = 100)
    birthday = models.DateField()
    gender = models.CharField(max_length = 10)
    #expertise = models.CharField(max_length = 100) 
    description = models.TextField()
    email = models.EmailField()
