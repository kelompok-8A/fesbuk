from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Profile

# Create your tests here.
class UserProfileTest(TestCase):
    def test_user_profile_url_is_exist(self):
        test = Profile.objects.create(name='test', birthday='1234-12-20', gender='male', description='test', email='test@test.com')
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_user_profile_using_index_func(self):
        test = Profile.objects.create(name='test', birthday='1234-12-20', gender='male', description='test', email='test@test.com')
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_calculate_age(self):
        birth_date = '2016-10-13'
